import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrderprogramGUI {
    private JButton AgodashiRamenButton;
    private JButton JiroRamenButton;
    private JButton TonkotsuRamenButton;
    private JButton MisoRamenButton;
    private JButton shioRamenButton;
    private JButton ShoyuRamenButton;
    private JTextArea textArea1;
    private JPanel root;
    private JTextArea textArea2;
    int sum=0;
    public static void main(String[] args) {
        JFrame frame = new JFrame("OrderprogramGUI");
        frame.setContentPane(new OrderprogramGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String foodName,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like order "+foodName+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            String currentText = textArea1.getText();
            JOptionPane.showMessageDialog(null,"Order for "+ foodName+" received.");
            textArea1.setText(currentText+"￥"+price + foodName+"\n");
            sum +=price;
            textArea2.setText("￥"+sum);
        }
    }
    public OrderprogramGUI() {
        AgodashiRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("AgodashiRamen",650);
                AgodashiRamenButton.setIcon(new ImageIcon(
                        this.getClass().getResource("PATH_TO_IMAGE")
                ));
            }
        });
        JiroRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("JiroRamen",800);
                JiroRamenButton.setIcon(new ImageIcon(
                        this.getClass().getResource("PATH_TO_IMAGE")
                ));
            }
        });
        ShoyuRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ShoyuRamen",600);
                ShoyuRamenButton.setIcon(new ImageIcon(
                        this.getClass().getResource("PATH_TO_IMAGE")
                ));
            }
        });
        shioRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ShioRamen",500);
                shioRamenButton.setIcon(new ImageIcon(
                        this.getClass().getResource("PATH_TO_IMAGE")
                ));
            }
        });
        TonkotsuRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("TonkotsuRamen",550);
                TonkotsuRamenButton.setIcon(new ImageIcon(
                        this.getClass().getResource("PATH_TO_IMAGE")
                ));
            }
        });
        MisoRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("MisoRamen",650);
                MisoRamenButton.setIcon(new ImageIcon(
                        this.getClass().getResource("PATH_TO_IMAGE")
                ));
            }
        });
    }
}
